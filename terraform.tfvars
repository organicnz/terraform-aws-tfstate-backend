# AWS Settings
aws_access_key = "{{ lookup('file', lookup('env','HOME') + '/.ssh/aws_access_key') }}"
aws_secret_key = "{{ lookup('file', lookup('env','HOME') + '/.ssh/aws_secret_key') }}"
aws_region     = "us-west-2"